var Product = require('../models/product');
var mongoose = require('mongoose');

mongoose.connect('localhost:27017/tiendaMusica');

var products = [
    new Product({
    banda : "Mercyful Fate",
    album : "Mercyful Fate",
    formato : "Vinilo",
    descripcion : "Es el primer EP de la banda danesa Mercyful Fate, nombrado en aquel momento Nuns Have No Fun. El álbum contiene cuatro temas, grabado y mezclado por el sello discográfico Stone Studio, en Roosendaal, Países Bajos. Fue lanzado a la venta en 1982 por la compañía RaveOn Records",
    precio : "30000",
    imagen : "/images/Mercyful Fate/img1.jpg",
    genero : "Heavy Metal"
  }),
  new Product({
    banda : "Mercyful Fate",
    album : "Melissa",
    formato : "CD",
    descripcion : "álbum debut de la banda danesa de heavy metal Mercyful Fate. Lanzado al mercado en 1983 por la compañía discográfica Roadrunner Records, este LP contiene siete temas de gran factura, entre los cuales se encuentra Satan's Fall",
    precio : "11000",
    imagen : "/images/Mercyful Fate/img2.jpg",
    genero : "Heavy Metal"
  }),
  new Product({
    banda : "Bathory",
    album : "Bathory",
    formato : "Cassete",
    descripcion : "primer álbum de la banda sueca de Black metal Bathory. Es considerado una de las primeras grabaciones que definieron el estilo black metal.",
    precio : "13500",
    imagen : "/images/Bathory/img1.jpg",
    genero : "Black Metal"
    
 }),
  new Product({
    banda : "Bathory",
    album : "The Return...",
    formato : "Vinilo",
    descripcion : "Segundo álbum de Bathory. Está influenciado por los emergentes géneros death metal y black metal.",
    precio : "22000",
    imagen :  "/images/Bathory/img2.jpg",
    genero : "Black Metal"
  }),
  new Product({
    banda : "Candlemass",
    album : "Epicus Doomicus Metallicus",
    formato : "CD",
    descripcion : " álbum debut de la banda de doom metal sueco Candlemass, lanzado el 10 de junio de 1986 con el sello discográfico Black Dragon records",
    precio : "9500",
    imagen : "/images/Candlemass/img1.jpg",
    genero : "Doom Metal"
  }),
  new Product({
    banda : "Candlemass",
    album : "Nightfall",
    formato : "Vinilo",
    descripcion : "Nightfall es el segundo álbum de la banda de Doom metal sueca Candlemass. Lanzado en 1987, es considerado como el mejor álbum de Doom metal jamás creado y el mejor disco de la banda. La razón de este éxito se debe en parte a los nuevos miembros que entraron a la banda; Messiah Marcolin (cantante), Lars Johansson (guitarra principal) y Jan Lindh (batería).",
    precio : "25000",
    imagen : "/images/Candlemass/img2.jpg",
    genero : "Doom Metal"
  }),
  new Product({
    banda : "Whiplash",
    album : "Power and Pain",
    formato : "Cassete",
    descripcion : "álbum debut de la banda de thrash metal de Nueva Jersey Whiplash. Fue lanzado en 1985 a través de Roadrunner Records.",
    precio : "9500",
    imagen : "/images/Whiplash/img1.jpg",
    genero : "Thrash Metal"
  }),
  new Product({
    banda : "Whiplash",
    album : "Ticket to Mayhem",
    formato : "CD",
    descripcion : "second studio album from New Jersey thrash metal band, Whiplash. It was released on Roadrunner Records in 1987",
    precio : "12000",
    imagen : "/images/Whiplash/img2.jpg",
    genero : "Thrash Metal"
  }),
  new Product({
    banda : "Benediction",
    album : "Subconscious Terror",
    formato : "CD",
    descripcion : "anda británica de death metal proveniente de Birmingham, Inglaterra, formada en febrero de 1989.",
    precio : "11000",
    imagen : "/images/Benediction/img1.jpg",
    genero : "Death Metal"
  }),
  new Product({
    banda : "Benediction",
    album : "The Grand Leveller",
    formato : "CD",
    descripcion : "anda británica de death metal proveniente de Birmingham, Inglaterra, formada en febrero de 1989.",
    precio : "9000",
    imagen : "/images/Benediction/img2.jpg",
    genero : "Death Metal"
  }),
  new Product({
    banda : "King Diamond",
    album : "Them",
    formato : "Vinilo",
    descripcion : "tercer álbum de estudio de la banda de heavy metal King Diamond, publicado el 18 de julio de 1988, por el sello discográfico Roadrunner Records. El álbum fue producido por el propio King Diamond.",
    precio : "26000",
    imagen : "/images/King Diamond/img1.jpg",
    genero : "Heavy Metal"
 }),
  new Product({
    banda : "King Diamond",
    album : "Conspiracy",
    formato : "Vinilo",
    descripcion : "álbum conceptual de la banda King Diamond. Salió al mercado el 19 de noviembre de 1989 a través de la discográfica Roadrunner Records.Este álbum es la segunda parte de la historia que inicio con Them, donde nos cuenta la aterradora historia de King, su hermana Missy, la diabólica abuela y una mansión llena de espíritus. Conspiracy tiene lugar 18 años después cuando King regresa a la mansión tras estar recluido en el mismo sanatorio que su abuela.",
    precio : "27000",
    imagen : "/images/King Diamond/img2.jpg",
    genero : "Heavy Metal"
  }),
  new Product({
    banda : "Napalm Death",
    album : "Scum",
    formato : "CD",
    descripcion : "the debut album by English grindcore band Napalm Death. It was released on 1 July 1987 through Earache Records.",
    precio : "9500",
    imagen : "/images/Napalm Death/img1.jpg",
    genero : "Grindcore"
  }),
  new Product({
    banda : "Napalm Death",
    album : "Harmony Corruption",
    formato : "CD",
    descripcion : " third album by Napalm Death, released in 1990 on Earache Records.The style of the album was more death metal than grindcore, featuring extremely heavy riffs and deep low vocals.",
    precio : "7500",
    imagen : "/images/Napalm Death/img2.jpg",
    genero : "Grindcore"
  }),
  new Product({
    banda : "Carcass",
    album : "Reek of Putrefaction",
    formato : "Vinilo",
    descripcion : "primer álbum de estudio de la banda inglesa de metal extremo Carcass, lanzado en julio de 1988 por el sello discográfico Earache Records.",
    precio : "30000",
    imagen : "/images/Carcass/img1.jpg",
    genero : "Goregrind"
  }),
  new Product({
    banda : "Carcass",
    album : "Symphonies of Sickness",
    formato : "CD",
    descripcion : "segundo álbum de la banda de metal extremo británica, Carcass. Fue lanzado a través de Earache Records en diciembre de 1989.Mientras que el álbum aún conserva el estilo goregrind del Reek of Putrefaction, el sonido se hizo más característico al death metal.",
    precio : "8600",
    imagen : "/images/Carcass/img2.jpg",
    genero : "Goregrind"
  }),
  new Product({
    banda : "Dissection",
    album : "The Somberlain",
    formato : "Cassete",
    descripcion : "álbum debut de la banda sueca de Blackened death metal Dissection, lanzado en 1993; considerado como muy influyente para el black metal y el death metal.La banda dedicó el álbum a Euronymous de Mayhem, quien fue asesinado ese mismo año por Varg Vikernes.",
    precio : "11000",
    imagen : "/images/Dissection/img1.jpg",
    genero : "Black Metal"
  }),
  new Product({
    banda : "Dissection",
    album : "Storm of the Light's Bane",
    formato : "Cassete",
    descripcion : "segundo álbum de la banda sueca de blackened death metal, Dissection. Fue lanzado en 1995 por Nuclear Blast Records, también se lanzó una edición especial digipack limitada a 500 copias. En 2002 fue lanzado de nuevo en digipack pero esta vez incluyendo el EP de 1997 Where Dead Angels Lie como bonus track. En 2006 fue de nuevo relanzado, esta vez por Black Horizon Music The End Records",
    precio : "11000",
    imagen : "/images/Dissection/img2.jpg",
    genero : "Black Metal"
  }),
  new Product({
    banda : "Rhapsody(of Fire)",
    album : "Legendary Tales",
    formato : "CD",
    descripcion : "primer álbum de Rhapsody. Fue lanzado en 1997 y es la primera parte de la Emerald Sword Saga. Gracias a este disco, Rhapsody se dio a conocer rápidamente en la escena del power metal sinfónico con ritmos medievales europeos.",
    precio : "9500",
    imagen : "/images/Rhapsody of Fire/img1.jpg",
    genero : "Power Metal"
  }),
  new Product({
    banda : "Rhapsody(of Fire)",
    album : "Dawn Of Victory",
    formato : "CD",
    descripcion : "Dawn of Victory es el tercer álbum del grupo de power metal, Rhapsody of Fire. Fue publicado en el 2000.",
    precio : "8990",
    imagen : "/images/Rhapsody of Fire/img2.jpg",
    genero : "Power Metal"
  }),
  new Product({
    banda : "Running Wild",
    album : "Gates to Purgatory",
    formato : "Vinilo",
    descripcion : "fue el album debut del grupo de Heavy metal Running Wild. Este álbum, lanzado el año 1984 en vinilo y luego reeditado en formato cd, se caracteriza por la temática oscura de sus letras.",
    precio : "45000",
    imagen : "/images/Running Wild/img1.jpg",
    genero : "Heavy Metal"
  }),
  new Product({
    banda : "Running Wild",
    album : "Under Jolly Roger",
    formato : "Vinilo",
    descripcion : "the third studio album by Running Wild. This album marked a stylistic turning point in which the group dropped the Satanic imagery they had previously used and adopted the pirate theme they would become known for, creating and influencing the later named Pirate metal subgenre of heavy metal in the 2000s in the process. The album's title comes from the famous Jolly Roger, the flag used by pirates to identify their ships. The album has sold 250,000 copies Worldwide.",
    precio : "27000",
    imagen : "/images/Running Wild/img2.jpg",
    genero : "Heavy Metal"
  }),
  new Product({
    banda : "Running Wild",
    album : "Under Jolly Roger",
    formato : "CD",
    descripcion : "the third studio album by Running Wild. This album marked a stylistic turning point in which the group dropped the Satanic imagery they had previously used and adopted the pirate theme they would become known for, creating and influencing the later named Pirate metal subgenre of heavy metal in the 2000s in the process. The album's title comes from the famous Jolly Roger, the flag used by pirates to identify their ships. The album has sold 250,000 copies Worldwide.",
    precio : "27000",
    imagen : "/images/Running Wild/img2.jpg",
    genero : "Heavy Metal"
  }),
  new Product({
    banda : "Running Wild",
    album : "Under Jolly Roger",
    formato : "Cassette",
    descripcion : "the third studio album by Running Wild. This album marked a stylistic turning point in which the group dropped the Satanic imagery they had previously used and adopted the pirate theme they would become known for, creating and influencing the later named Pirate metal subgenre of heavy metal in the 2000s in the process. The album's title comes from the famous Jolly Roger, the flag used by pirates to identify their ships. The album has sold 250,000 copies Worldwide.",
    precio : "27000",
    imagen : "/images/Running Wild/img2.jpg",
    genero : "Heavy Metal"
  })

];


var aux = 0;
for (var i = 0; i < products.length; i++) {
    products[i].save(function(err, result) {
        aux++;
        if (aux === products.length) {
            exit();
        }
    });
}

function exit() {
    mongoose.disconnect();
}