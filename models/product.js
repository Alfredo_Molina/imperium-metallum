var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    banda: {type: String, required: true},
    album: {type: String, required: true},
    formato: {type: String, required: true},
    descripcion: {type: String, required: true},
    precio: {type: Number, required: true},
    imagen: {type: String, required: true},
    genero: {type: String, required: true}
});

 
module.exports = mongoose.model('Product', schema);