module.exports = function Cart(defaultCart) {
    this.items = defaultCart.items || {};
    this.totalCantidad = defaultCart.totalCantidad || 0;
    this.totalPrecio = defaultCart.totalPrecio || 0;

    this.agregar = function (item, id) {
        var storedItem = this.items[id];
        if (!storedItem) {
            storedItem = this.items[id] = { item: item, cantidad: 0, precio: 0 ,cantidad2: 0};
        }
        storedItem.cantidad++;
        storedItem.precio = storedItem.item.precio * storedItem.cantidad;
        this.totalCantidad++;
        this.totalPrecio += storedItem.item.precio;
    };
    //funcion remover item
    this.removerUno = function (id) {
        this.items[id].cantidad--;
        this.items[id].precio -= this.items[id].item.precio;
        this.totalCantidad--;
        this.totalPrecio -= this.items[id].item.precio;
        //borra el item al tener una cantidad menor/igual a 0
        if (this.items[id].cantidad <= 0) {
            delete this.items[id];
        }
    };

    this.remover = function (id) {
        this.totalCantidad -= this.items[id].cantidad;
        this.totalPrecio -= this.items[id].precio;
        delete this.items[id];
    };
    //pasar objetos(items) a un Array
    this.generarArray = function () {
        var arr = [];
        for (var id in this.items) {
            arr.push(this.items[id]);
        }
        return arr;
    };
};