function login() {
	document.getElementById('content').innerHTML =
		'<div id="text-divisor">' +
		'Iniciar Session' +
		'</div><br>' +
		'<div class="row" id="form-login">' +
		'<div class="col-md-4 col-md-offset-4">' +
		'<form action="/" method="post">' + /*Editar en el futuro */
		'<div class="form-group">' +
		'<label for="username">Usuario: </label>' +
		'<input type="text" id="username" name="username" class="form-control">' +
		'</div>' +
		'<div class="form-group">' +
		'<label for="password">Contraseña: </label>' +
		'<input type="password" id="password" name="password" class="form-control">' +
		'</div>' +
		'<div class="btn-center">' +
		'<button type="submit" class="btn">Registrarse</button>' +
		'</div>' +
		'</form>' +
		'</div>' +
		'</div><br>' +
		'<div id="text-divisor">' +
		'<h1></h1>' +
		'</div><br>';
}


function register() {
	var modal = "";
	modal =
		'<!-- Modal -->' +
		'<div class="modal fade" id="myModal" role="dialog">' +
		'	<div class="modal-dialog">' +
		'	  <!-- Modal content-->' +
		'	  <div class="modal-content">' +
		'		<div class="modal-header">' +
		'		  <button type="button" class="close" data-dismiss="modal">&times;</button>' +
		'		  <h4 class="modal-title">REGISTRADO!!</h4>' +
		'		</div>' +
		'		<div class="modal-body">' +
		'		  <p>Tus datos han quedado registrado en el sitio, ahora puedes iniciar Sesion</p>' +
		'		</div>' +
		'		<div class="modal-footer">' +
		'		  <button type="button" class="btn" data-dismiss="modal" onclick="loadFeatured()">Ok</button>' +
		'		</div>' +
		'	  </div>' +
		'	</div>' +
		'</div>';

	document.getElementById('content').innerHTML =
		'<div class="row" id="form-registro">' +
		'<div id="text-divisor">' +
		'Crear Nueva Cuenta' +
		'</div><br>' +
		'<div class="col-md-9 col-md-offset-1">' +
		'<div class="form-horizontal">' +
		'<form onsubmit="return launchModal()">' +
		'<div class="form-group">' +
		'<label for="username" class="control-label col-sm-2">NickName: </label>' +
		'<div class="col-sm-8">' +
		'<input type="text" id="username" class="form-control">' +
		'</div>' +
		'</div >' +
		'<div class="form-group">' +
		'<label for="password" class="control-label col-sm-2">Contraseña: </label>' +
		'<div class="col-sm-8">' +
		'<input type="password" id="password" class="form-control">' +
		'</div>' +
		'</div>' +
		'<div class="form-group">' +
		'<label for="nombre" class="control-label col-sm-2">Nombre: </label>' +
		'<div class="col-sm-8">' +
		'<input type="text" id="nombre" class="form-control">' +
		'</div >' +
		'</div>' +
		'<div class="form-group">' +
		'<label for="apellido" class="control-label col-sm-2">Apellido: </label>' +
		'<div class="col-sm-8">' +
		'<input type="text" id="apellido" class="form-control">' +
		'</div>' +
		'</div>' +
		'<div class="form-group">' +
		'<label for="email" class="control-label col-sm-2">E-Mail: </label>' +
		'<div class="col-sm-8">' +
		'<input type="email" id="email" class="form-control">' +
		'</div>' +
		'</div>' +
		'<div class="form-group">' +
		'<label for="direccion" class="control-label col-sm-2">Direccion: </label>' +
		'<div class="col-sm-8">' +
		'<input type="text" id="direccion" class="form-control">' +
		'</div>' +
		'</div>' +
		'<div class="btn-center"><button type="submit" class="btn" >Registrarse</button></div>' +
		'</form>' +
		'</div>' +
		'</div>' +
		'</div><br>' +
		'<div id="text-divisor">' +
		'<h1></h1>' +
		'</div>' + modal;
}

function launchModal() {
	$("#myModal").modal();
	return false;
}

function loadProductos() {
	src="/socket.io/socket.io.js"
	var loadedProductos = "";
	var io = io.connect("/");

	io.on("productos", function (data) {
		var totalPro = data.length;

		for (var i = 0; i < totalPro; i++) {

			loadedProductos +=
				'<div class="col-sm-3">' +
				'<div class="panel">' +
				'<div class="panel-body" id="image-product">' +
				'<a href="#">' +
				'<img src="/images/img' + [i + 1] + '.jpg" alt="..." class="img-responsive">' +
				'</a>' +
				'</div>' +
				'<div class="panel-footer">' +
				'<p class="band-text">Banda:' + data[i].nombre_banda + '</p>' +
				'<p class="album-text">Album: ' + data[i].nombre_album + '</p>' +
				'<p class="price">Precio: $ ' + data[i].precio + '</p>' +
				'<input type="submit" class="btn" id="product' + [i] + '" value="Añadir">' +
				'</div>';
		};
	});
	document.getElementById('content1').innerHTML = loadedProductos;
}