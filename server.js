var express = require('express');
var app = express();
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongo = require('mongodb');
var mongoose = require('mongoose');
var mongoStore = require('connect-mongo')(session);

mongoose.connect('mongodb://localhost/tiendaMusica');
var db = mongoose.connection;

var routes = require('./routes/index');
var users = require('./routes/users');



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

// Express Session y Stored session
app.use(session({
  secret: 'secret',
  saveUninitialized: false,
  resave: false,
  store: new mongoStore({ mongooseConnection: mongoose.connection }),
  cookie: { maxAge: 120 * 60 * 1000 }//la session expira en 2 horas
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Express Validator
app.use(expressValidator({
  errorFormatter: function (param, msg, value) {
    var namespace = param.split('.')
      , root = namespace.shift()
      , formParam = root;

    while (namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param: formParam,
      msg: msg,
      value: value
    };
  }
}));

// connectar Flash
app.use(flash());

// variables globales
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;//
  res.locals.session = req.session;//
  next();
});

//routes
app.use('/users', users);
app.use('/', routes);

//redireccion a not found si no encuentra la direccion
app.get('*', function (req, res) {
  res.render('pages/notFound');
});


app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function () {
  console.log('Servidor iniciado en el puerto ' + app.get('port'));
});