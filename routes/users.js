var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');


router.get('/login', function (req, res) {
    res.render('pages/login');
});
router.get('/profile', function (req, res) {
    res.render('pages/profile');
});

router.get('/register', function (req, res) {
    var errors = req.validationErrors();
    console.log("/register metodo get " + errors);
    res.render('pages/register', {
        errors: errors
    });
});

// registrar usuario
router.post('/register', function (req, res, next) {
    var name = req.body.name;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var password2 = req.body.password2;

    // validaciones
    req.checkBody('name', 'Nombre requerido').notEmpty();
    req.checkBody('email', 'Email requerido').notEmpty();
    req.checkBody('email', 'Email no valido').isEmail();
    req.checkBody('username', 'Username requerido').notEmpty();
    req.checkBody('password', 'Contraseña requerida').notEmpty();
    req.checkBody('password2', 'Las Contraseñas no coniciden').equals(req.body.password);

    var errors = req.validationErrors();
    console.log("register metodo post " + errors);

    if (errors) {
        res.render('pages/register', {
            errors: errors
        });
    } else {
        var newUser = new User({
            name: name,
            email: email,
            username: username,
            password: password
        });

        User.createUser(newUser, function (err, user) {
            if (err) throw err;
            console.log(user);
        });

        req.flash('success_msg', 'Ya estas registrado, ahora puede iniciar sesion!');

        res.redirect('/users/login');
    }
});

passport.use(new LocalStrategy(
    function (username, password, done) {
        User.getUserByUsername(username, function (err, user) {
            if (err) throw err;
            if (!user) {
                return done(null, false, { message: 'El usuario no existe' });
            }

            User.comparePassword(password, user.password, function (err, isMatch) {
                if (err) throw err;
                if (isMatch) {
                    return done(null, user);
                } else {
                    return done(null, false, { message: 'Contraseña incorrecta' });
                }
            });
        });
    }));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.getUserById(id, function (err, user) {
        done(err, user);
    });
});

router.post('/login',
    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/users/login',
        badRequestMessage: 'Ingrese todos los datos',
        failureFlash: true
    }),
    function (req, res) {
        res.redirect('/productos');
    });

router.get('/logout', function (req, res) {
    req.logout();

    req.flash('success_msg', 'Has cerrado Sesion');

    res.redirect('/users/login');
});


module.exports = router;