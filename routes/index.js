var express = require('express');
var router = express.Router();
var ProductoJSON = require('../productos.json');
var Producto = require('../models/product');
var Cart = require('../models/cart');

router.get('/', function (req, res) {
    Producto.find(function (err, docs) {
        var productos = docs;
        res.render('pages/index', { productos});
    });
});

//ver Carrito
router.get('/shoppingCart', function (req, res) {
    console.log("sin Productos " + req.session.cart);
    var productos = null;
    if (!req.session.cart) {
        return res.render('pages/shoppingCart', { productos: null });
    }
    var cart = new Cart(req.session.cart);

    res.render('pages/shoppingCart', {
        productos: cart.generarArray(),
        precioTotal: cart.totalPrecio
    });
    console.log("productos " + productos);
});

////agregar al carrito
router.get('/agregar/:id', function (req, res) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});
    Producto.findById(productId, function (err, product) {
        if (err) {
            return res.redirect('/productos');
        }
        cart.agregar(product, product.id);
        req.session.cart = cart;
        console.log("Agregar " + product);
        res.redirect('/productos');

    })
});
//function remover por 1
router.get('/removerUno/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.removerUno(productId);
    req.session.cart = cart;
    res.redirect('/shoppingCart');
});
//function remover todo
router.get('/remover/:id', function (req, res, next) {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.remover(productId);
    req.session.cart = cart;
    res.redirect('/shoppingCart');
});

////Detalle producto
router.get('/detalle/:id', function (req, res) {
    var productId = req.params.id;
    Producto.findById(productId, function (err, docs) {
        var producto = docs;
        if (err) {
            return res.redirect('/productos');
        }
        res.render('pages/details', { producto });
        console.log("producto  " + producto);
    });
});
//Mostrar todos los productos
router.get('/productos', function (req, res) {
    Producto.find(function (err, docs) { //funcion que pasa todos los productos de la db a un doc
        var productos = docs;
        var todo = docs;
        var banda = typeof req.query.banda !== "undefined" ? req.query.banda : "Todos";
        var formato = typeof req.query.formato !== "undefined" ? req.query.formato : "Todos";
        var genero = typeof req.query.genero !== "undefined" ? req.query.genero : "Todos";
        var i = 0;
        var pageSize = 6;
        var pageCount = 0;
        var currentPage = typeof req.query.page !== "undefined" ? req.query.page - 1 : 0;
        var prductosFiltrados = [];
        var productosArray = [];

        for (i in productos) {
            if ((productos[i].banda === banda || banda === 'Todos') &&
                (productos[i].formato === formato || formato === 'Todos') &&
                (productos[i].genero === genero || genero === 'Todos')) {
                prductosFiltrados.push(productos[i]);
            }
        }
        while (prductosFiltrados.length > 0) {
            productosArray.push(prductosFiltrados.splice(0, pageSize));
        }
        pageCount = Math.ceil(productosArray.length);

        res.render('pages/productos',
            {
                productos: productosArray.length > 0 ? productosArray[currentPage] : productosArray,
                todo: todo,//devuelve todos los productos
                content: [banda, formato, genero],
                pageCount: pageCount,
                currentPage: currentPage
            });

    });

});

router.post('/shoppingCart', function (req, res) {
    console.log("req.body: " + req.body);
    var data = {
        banda: req.body.banda,
        album: req.body.album,
        genero: req.body.genero,
        precio: req.body.precio
    }
    var items = [data];
    console.log(items);

    res.render('pages/shoppingCart', {

    });

});

module.exports = router;